import React, { useEffect, useState, Component } from "react";
import './App.css';
import logo from './logo.svg';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: new Date().toLocaleString()
    };
  }
  componentDidMount() {
    this.intervalID = setInterval(
      () => this.tick(),
      1000
    );
  }
  componentWillUnmount() {
    clearInterval(this.intervalID);
  }
  tick() {
    this.setState({
      time: new Date().toLocaleString()
    });
  }
  render() {
    return (
      <div className="App">
      <header className="App-header">
        
        <p>
          {this.state.time}
        </p>
        
      </header>
    </div>
    );
  }
}

export default App;